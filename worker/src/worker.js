require('dotenv').config({
    silent: true
})
const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 3600
const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const backendUrl = process.env.SABTESTRUNNER_BACKEND_ENDPOINT
io.on('connection', async function(socket) {

});

const backendSocket = require('socket.io-client')(backendUrl)

backendSocket.on('getProjectsNames', data => {
    backendSocket.emit('getProjectsNames_' + data.__id, {
        projectNames: ["GEORED", "API_GEORED", process.env.PORT]
    })
})

backendSocket.on('connect', async function(socket) {
    console.log('Backend connection established')
});
backendSocket.on('disconnect', async function(socket) {

});

http.listen(PORT, function() {
    console.log(`Listening on ${PORT}`)
});

const PROJECTS = process.env.SABTESTRUNNER_PROJECTS.split(",")

console.log({
    backendUrl
})