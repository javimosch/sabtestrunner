require('dotenv').config({
    silent: true
})
const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 3500
const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http);

app.sockets = app.sockets || {}
app.getSockets = () => {
    return Object.keys(app.sockets).map(k => (app.sockets[k]))
}



app.workers = {
    tell(name) {
        var args = Array.prototype.slice.call(arguments);

        async function execute() {
            var args = Array.prototype.slice.call(arguments);
            args.unshift(1)
            const responses = await Promise.all(app.getSockets().map(socket => {
                return socketFunction(socket, name, args)
            }))
            return responses;
        }

        return {
            async mergeBy(name, defaultValue = []) {

                const responses = await execute.apply(this, args)

                return responses.reduce((acum, item) => {
                    if (acum instanceof Array) {
                        acum = acum.concat(item[name])
                        return acum
                    }
                    if (typeof acum === 'object') {
                        acum = {
                            ...acum,
                            ...item[name]
                        }
                    }
                }, defaultValue)
            }
        }
    }
}

function socketFunction(socket, name, args) {
    return new Promise((resolve, reject) => {
        const id = require('nanoid')()
        socket.once(name + '_' + id, (res) => {
            if (res.error || res.err) return reject(res.error || res.err)
            resolve(res)
        })
        socket.emit(name, {
            __id: id,
            args
        })
    })
}

io.on('connection', async function(socket) {
    app.sockets[socket.id] = socket;
    socket.on('disconnect', async function(socket) {
        delete app.sockets[socket.id]
    });
});




require('funql-api').middleware(app, {
    /*defaults*/
    getMiddlewares: [],
    postMiddlewares: [],
    allowGet: true,
    allowOverwrite: false,
    attachToExpress: false,
    allowCORS: true,
    api: {
        async hello(name) {
            let projectNames = await app.workers.tell('getProjectsNames').mergeBy('projectNames', [])
            return projectNames.filter((item, index) => {
                return projectNames.indexOf(item) == index
            })
        }
    }
})

http.listen(PORT, function() {
    console.log(`Listening on ${PORT}`)
});